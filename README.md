# buildroot

# buildroot
para el desarrollo de este proyeco se tuvo que instalar vagrant y vitual box, una vez hecho eso se descargo el build root y se modifico el archivo vagrant file en la sección shell para que se pudiera instalar los siguientes servicios por default:

-nginx
-python3
-pip 
-postgresql

una vez hecho esto se le vanto nuestra maquina virtual con esos servicios, eso se hizo mediante el comando :

<code>
	vagrant up
</code>

Y para poder conectarnos a nuestra maquina virtual utilizamos el siguiente comando:
<code>
	vagrant ssh
</code>


Una vez que ya entramos en nuetra maquina virtual cremos nuestro ambiente virtual el cual lo haremos de la siguiente manera:
<code>
	$ python3 -m venv .env
</code>

para poder activar el ambiente virtual lo haremos de la siguiente forma:
<code>
	$source .env/bin/activate
</code>

Una vez activado nos vamos a la carpeta pokemon en la cual  estara nuestro proyecto de djanago, tendremos que correr nuestro servidor de django de la siguiente manera:
<code>
	$ python3 manage.py runserver 0.0.0.0:8000
</code>

ya hecho lo anterior podremos ingresar a la pagína de la siguiente forma:
<code>
	https://55.55.55.5:8000
<code>

ahora lo que resta ver  la pagína donde podremos hacer lecturas y escrituras en la base de datos, esto lo haremos entrando a las siguientes urls 
<code>
	https://55.55.55.5:8000/verPokemon
	https://55.55.55.5:8000/agregarPokemon
</code>
 
link de la maquina virtual
<https://drive.google.com/file/d/1Safy1b0ZLApeqNq27BwmS5W22T8_H0QW/view?usp=sharing>

user : vagrant
pass : vagrant


